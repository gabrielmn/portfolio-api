import { HttpRequest, InvocationContext } from '@azure/functions';
import { httpGetCertificates } from '../src/functions/httpGetCertificates';

describe('httpGetCertificates Function', () => {

	it('Accept-Language header is missing', async () => {

		// Mock HttpRequest and Context
		const request: HttpRequest = new HttpRequest({
			url: 'http://localhost:7071/api/certificates',
			method: 'GET',
			headers: {}
		});

		const context = new InvocationContext({
			functionName: 'httpGetCertificates'
		});

		// Invoke the function
		const response = await httpGetCertificates(request, context);

		// Assertions
		expect(response.status).toBe(400);
	});

});
