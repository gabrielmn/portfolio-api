import { HttpRequest, InvocationContext } from '@azure/functions';
import { httpGetProjects } from '../src/functions/httpGetProjects'; 

describe('httpGetProjects Function', () => {

  it('Accept-Language header is missing', async () => {

    // Mock HttpRequest and Context
    const request: HttpRequest = new HttpRequest({
      url: 'http://localhost:7071/api/certificates',
      method: 'GET',
      headers: {}
    });

    const context = new InvocationContext({
      functionName: 'httpGetProjects'
    });

    // Invoke the function
    const response = await httpGetProjects(request, context);

    // Assertions
    expect(response.status).toBe(400);
  });

});