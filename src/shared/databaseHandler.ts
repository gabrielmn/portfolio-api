import { connect, Request, ConnectionPool, config, Transaction } from 'mssql';
// Re-export types from 'mssql'
export {
    BigInt,
    NVarChar,
    DateTime,
    Int,
    Decimal,
    Bit,
    Date,
    ConnectionPool,
    Request,
    Transaction,
    Table,
    ConnectionError,
    TransactionError,
    RequestError
} from 'mssql';

const server = process.env['MSSQL_SERVER'];
if (server === undefined)
    throw new Error('Environment variable server is not defined')

const sqlConfig: config = {

    server: server,
    database: process.env["MSSQL_DATABASE"],
    user: process.env["MSSQL_READONLY_API_USER"],
    password: process.env["MSSQL_READONLY_API_USER_PASSWORD"],
    options: {
        encrypt: true, // for azure
        trustServerCertificate: true // change to true for local dev / self-signed certs
    }
};

/**
 * 
 * Establish a connection with the database.
 *
 * @param options An object containing database name, user, and password.
 * @returns a new connection.
 */
export async function createConnection({ database, user, password }: { database?: string, user?: string, password?: string }): Promise<ConnectionPool> {

    // Clone the sqlConfig to avoid mutating the original object
    const newConfig = { ...sqlConfig };

    // Set the database if provided
    if (database) {
        newConfig.database = database;
    }

    // Set the user and password if both are provided
    if (user && password) {
        newConfig.user = user;
        newConfig.password = password;
    }

    // Establish and return the database connection
    return await connect(newConfig);
}

/**
 * 
 * End the connection with the database. 
 *
 * @param {*} connection - connection with the database.
 */
export function closeConnection(connection: ConnectionPool) {

    if (connection) {
        connection.close();
    }

}

/**
 * Creates a new SQL request object.
 * This function checks if the provided connection is either a connection pool or a transaction,
 * and returns a new Request object based on that connection.
 * 
 * @param {ConnectionPool | Transaction} connection - The connection pool or transaction to be used for the request.
 * @returns {Request} A new SQL request object configured with the given connection.
 * @throws {Error} If no connection is provided.
 */
export function createRequest(connection: ConnectionPool | Transaction): Request {

    // Check if the connection object is provided
    if (!connection)
        throw new Error('Connection must be established before creating a request.');

    // If the connection is an instance of ConnectionPool, create a Request with the ConnectionPool
    if (connection instanceof ConnectionPool)
        return new Request(connection);

    // If the connection is an instance of Transaction, create a Request with the Transaction
    if (connection instanceof Transaction)
        return new Request(connection);
}

/**
 * Creates a new SQL transaction object.
 * This function initializes a new Transaction using the provided connection pool.
 * 
 * @param {ConnectionPool} connection - The connection pool to be used for the transaction.
 * @returns {Transaction} A new Transaction object configured with the given connection pool.
 * @throws {Error} If no connection pool is provided.
 */
export function createTransaction(connection: ConnectionPool): Transaction {

    // Check if the connection pool object is provided
    if (!connection)
        throw new Error('Connection must be established before creating a transaction.');

    // Create and return a new Transaction with the provided ConnectionPool
    return new Transaction(connection);
}
