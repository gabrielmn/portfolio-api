export class GitlabError extends Error {
    constructor(message: string) {
        super(message);
        this.name = "GitlabError";
        Object.setPrototypeOf(this, new.target.prototype);
    }
}