export type GitlabProject = {
    id: number;
    avatar_url: string | null;
    name: string;
    topics: string[];
    description: string | null;
    web_url: string;
    updated_at: string;

}