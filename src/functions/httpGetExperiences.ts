import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";
import {
    createConnection,
    closeConnection,
    ConnectionPool,
    createRequest,
    Request,
    ConnectionError,
    RequestError,
    NVarChar
} from "../shared/databaseHandler";

import { BadRequestError } from "../shared/BadRequestError";

export async function httpGetExperiences(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    
    let connection: ConnectionPool | null = null;

    try {

        // Retrieve the language preference from the Accept-Language header
        const language = request.headers.get('accept-language');
        if (!language)
            throw new BadRequestError("The 'Accept-Language' header is missing and is required for this request.");

        connection = await createConnection({
            user: process.env["MSSQL_API_READ_ONLY_USER"],
            password: process.env["MSSQL_API_READ_ONLY_USER_PASSWORD"]
        });

        const experiencesRequest: Request = createRequest(connection);
        experiencesRequest.input('language', NVarChar(8), language);
        const experiencesResult = await experiencesRequest.query(`
            SELECT 
            [experience].[experiences].[id],
            [experience].[experiences].[type], 
            [experience].[experiences].[employment_type], 
            [experience].[experiences].[company],
            [experience].[experiences].[position],
            [experience].[experiences].[start_date],
            [experience].[experiences].[end_date],
            [experience].[experiences].[image],
            (
                SELECT
                    [experience].[experience_responsibilities].[id],
                    [experience].[experience_responsibilities].[responsibility]
                FROM [experience].[experience_responsibilities] 
                WHERE [experience].[experience_responsibilities].[experience_id] = [experience].[experiences].[id] AND [experience].[experience_responsibilities].[language] = @language
                FOR JSON PATH
            ) AS [responsibilities]
            FROM [experience].[experiences];
        `)

        const experiences = [];

        for (const experience of experiencesResult.recordset) {
            const temp = {
                id: experience.id,
                type: experience.type,
                employmentType: experience.employment_type,
                company: experience.company,
                position: experience.position,
                startDate: experience.start_date,
                endDate: experience.end_date,
                image: experience.image,
                responsibilities: JSON.parse(experience.responsibilities)
            };
            experiences.push(temp);
        }

        return {
            status: 200,
            body: JSON.stringify(experiences)
        }
    }
    catch (error) {

        if (error instanceof BadRequestError){
            context.log(`Something went wrong with the request:\n\s${error}`);
            return{
                status: 400,
                body: error.message
            }
        }
        if (error instanceof ConnectionError) {
            context.log(`Something went wrong with the database connection:\n\s${error}`);
            return{
                status: 500
            }
        }
        else if (error instanceof RequestError) {
            context.log(`Something went wrong with the database request:\n\s${error}`);
            return{
                status: 500
            }
        }

        context.log(`Something went wrong: ${error}`);
        return {
            status: 500
        };

    }
    finally {

        if (connection)
            closeConnection(connection);

    }
};

app.http('httpGetExperiences', {
    methods: ['GET'],
    authLevel: 'function',
    handler: httpGetExperiences,
    route: 'experiences'
});