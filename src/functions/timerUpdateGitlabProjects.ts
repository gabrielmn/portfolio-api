import { app, InvocationContext, Timer } from "@azure/functions";
import {
    createConnection,
    closeConnection,
    ConnectionPool,
    createRequest,
    createTransaction,
    Transaction,
    Request,
    NVarChar,
    BigInt,
    ConnectionError,
    TransactionError,
    RequestError
} from "../shared/databaseHandler";

import { Language } from "../shared/Language.type";
import { GitlabProject } from "../shared/GitlabProject.type";
import { GitlabError } from "../shared/GitlabError";

const languages: Language[] = [
    { ietf: "en-us", azure: "en" },
    { ietf: "pt-br", azure: "pt" }
]

export async function timerUpdateGitlabProjects(myTimer: Timer, context: InvocationContext): Promise<void> {

    let connection: ConnectionPool | null = null;
    let transaction: Transaction | null = null;

    try {

        const today = new Date();
        const lastWeekMonday = new Date();
        lastWeekMonday.setDate(today.getDate() - 7);

        const response = await fetch(`https://gitlab.com/api/v4/users/gabrielmn/projects`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'private-token': process.env["GITLAB_API_TOKEN"]
            }
        });

        if (response.status !== 200)
            throw new GitlabError(`Gitlab API error: ${response.status}\n\s${response.body}`);

        const projects: GitlabProject[] = await response.json();
        if (projects.length === 0)
            throw new GitlabError(`Gitlab API error: ${response.status}\n\s${response.body}`);

        connection = await createConnection({
            user: process.env["MSSQL_API_FULL_ACCESS_USER"],
            password: process.env["MSSQL_API_FULL_ACCESS_USER_PASSWORD"]
        });

        transaction = createTransaction(connection);

        await transaction.begin();

        const currentProjectsRequest: Request = createRequest(transaction);
        const currentProjectsResult = await currentProjectsRequest.query('SELECT [gitlab_id] FROM [project].[projects];');

        const newProjects = projects.filter(project => !currentProjectsResult.recordset.some(record => parseInt(record.gitlab_id) === project.id));
        
        const outdatedProject = currentProjectsResult.recordset.filter(record => !projects.some(project => project.id === parseInt(record.gitlab_id)));
        
        const updateProjects = projects.filter(project => new Date(project.updated_at).getTime() > lastWeekMonday.getTime());

        for (const project of newProjects) {

            if (
                project.id === null ||
                project.name === null ||
                project.avatar_url === null ||
                project.description === null ||
                project.topics.length === 0 ||
                project.web_url === null
            )
                continue

            const projectRequest: Request = createRequest(transaction);

            projectRequest.input('gitlab_id', BigInt, project.id);
            projectRequest.input('name', NVarChar(2556), project.name);
            projectRequest.input('image', NVarChar(2048), project.avatar_url);
            projectRequest.input('repository', NVarChar(2048), project.web_url);

            const projectResult = await projectRequest.query(`INSERT INTO [project].[projects] ([gitlab_id], [name], [image], [repository]) VALUES ( @gitlab_id, @name, @image, @repository); SELECT @@IDENTITY AS id;`);

            if (projectResult.rowsAffected[0] === 1) {

                const projectId = projectResult.recordset[0].id;

                for (const language of languages) {

                    let translatedDescription = project.description;

                    if (language.ietf !== "en-us") {
                        const response = await fetch(`https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&from=en&to=${language.azure}`, {
                            method: 'POST',
                            headers: {
                                'Ocp-Apim-Subscription-Key': process.env["AZURE_TRANSLATE_API_KEY"],
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify([{ 'text': project.description }])
                        });

                        if (response.status === 200) {
                            const body = await response.json();
                            translatedDescription = body[0].translations[0].text;
                        }
                    }

                    const projectDescriptionRequest: Request = createRequest(transaction);

                    projectDescriptionRequest.input('project_id', BigInt, projectId);
                    projectDescriptionRequest.input('description', NVarChar(2048), translatedDescription);
                    projectDescriptionRequest.input('language', NVarChar(8), language.ietf);

                    const projectDescriptionResult = await projectDescriptionRequest.query(`INSERT INTO [project].[project_descriptions] ([project_id], [description], [language]) VALUES ( @project_id, @description, @language); SELECT @@IDENTITY AS id;`);


                }

                for (const topic of project.topics) {

                    const projectTopicsRequest: Request = createRequest(transaction);

                    projectTopicsRequest.input('project_id', BigInt, projectId);
                    projectTopicsRequest.input('topic', NVarChar(32), topic);

                    const projectTopicsResult = await projectTopicsRequest.query(`INSERT INTO [project].[project_topics] ([project_id], [topic]) VALUES ( @project_id, @topic); SELECT @@IDENTITY AS id;`);
                }

            }

        };

        for (const project of updateProjects) {

            if (
                project.id === null ||
                project.name === null ||
                project.avatar_url === null ||
                project.description === null ||
                project.topics.length === 0 ||
                project.web_url === null
            ) {
                outdatedProject.push({ gitlab_id: `${project.id}` });
                continue;
            }

            const projectRequest: Request = createRequest(transaction);

            projectRequest.input('gitlab_id', BigInt, project.id);
            projectRequest.input('name', NVarChar(2556), project.name);
            projectRequest.input('image', NVarChar(2048), project.avatar_url);
            projectRequest.input('repository', NVarChar(2048), project.web_url);

            const projectResult = await projectRequest.query(`UPDATE [project].[projects] SET [name] = @name, [image] = @image, [repository] = @repository OUTPUT [inserted].[id] WHERE [gitlab_id] = @gitlab_id; SELECT @@ROWCOUNT AS affectedRows;`);

            if (projectResult.rowsAffected[0] === 1) {

                const projectId = projectResult.recordset[0].id;

                for (const language of languages) {

                    let translatedDescription = project.description;

                    if (language.ietf !== "en-us") {
                        const response = await fetch(`https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&from=en&to=${language.azure}`, {
                            method: 'POST',
                            headers: {
                                'Ocp-Apim-Subscription-Key': process.env["AZURE_TRANSLATE_API_KEY"],
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify([{ 'text': project.description }])
                        });

                        if (response.status === 200) {
                            const body = await response.json();
                            translatedDescription = body[0].translations[0].text;
                        }
                    }

                    const projectDescriptionRequest: Request = createRequest(transaction);

                    projectDescriptionRequest.input('project_id', BigInt, projectId);
                    projectDescriptionRequest.input('description', NVarChar(2048), translatedDescription);
                    projectDescriptionRequest.input('language', NVarChar(8), language.ietf);

                    const projectDescriptionResult = await projectDescriptionRequest.query(`UPDATE [project].[project_descriptions] SET [description] = @description WHERE [project_id] = @project_id AND [language] = @language; SELECT @@ROWCOUNT AS affectedRows;`);
                }

                const projectTopicsRequest: Request = createRequest(transaction);
                projectTopicsRequest.input('project_id', BigInt, projectId);
                const projectTopicsResult = await projectTopicsRequest.query(`SELECT [id], [topic] FROM [project].[project_topics] WHERE [project_id] = @project_id;`);

                const toBeInserted = project.topics.filter(item => !projectTopicsResult.recordset.some(record => record.topic === item));

                const toBeDeleted = projectTopicsResult.recordset.filter(item => !project.topics.includes(item.topic));

                for (const { topic } of toBeDeleted) {

                    const projectTopicsRequest: Request = createRequest(transaction);

                    projectTopicsRequest.input('project_id', BigInt, projectId);
                    projectTopicsRequest.input('topic', NVarChar(32), topic);

                    const projectTopicsResult = await projectTopicsRequest.query(`DELETE FROM [project].[project_topics] WHERE [project_id] = @project_id AND [topic] = @topic; SELECT @@ROWCOUNT AS affectedRows;`);
                }

                for (const topic of toBeInserted) {

                    const projectTopicsRequest: Request = createRequest(transaction);

                    projectTopicsRequest.input('project_id', BigInt, projectId);
                    projectTopicsRequest.input('topic', NVarChar(32), topic);

                    const projectTopicsResult = await projectTopicsRequest.query(`INSERT INTO [project].[project_topics] ([project_id], [topic]) VALUES ( @project_id, @topic); SELECT @@IDENTITY AS id;`);

                }

            }

        };

        for (const project of outdatedProject) {
            const projectRequest: Request = createRequest(transaction);
            projectRequest.input('gitlab_id', BigInt, project.gitlab_id);

            const projectResult = await projectRequest.query(`DELETE FROM [project].[projects] WHERE [gitlab_id] = @gitlab_id; SELECT @@ROWCOUNT AS affectedRows;`);
        }

        await transaction.commit()

    }
    catch (error) {

        if (error instanceof GitlabError) {
            context.log(`Something went wrong with the gitlab api call:\n\s${error}`);
        }
        else if (error instanceof ConnectionError) {
            context.log(`Something went wrong with the database connection:\n\s${error}`);
        }
        else if (error instanceof TransactionError) {
            context.log(`Something went wrong with the database transaction:\n\s${error}`);
            transaction.rollback();
        }
        else if (error instanceof RequestError) {
            context.log(`Something went wrong with the database request:\n\s${error}`);
            transaction.rollback();
        }
        else {
            context.log(`Something went wrong: ${error}`);
        }

    }
    finally {

        if (connection)
            closeConnection(connection);

    }
}

app.timer('timerUpdateGitlabProjects', {
    schedule: '0 0 0 * * 1',
    handler: timerUpdateGitlabProjects
});