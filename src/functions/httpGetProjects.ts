import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";
import {
    createConnection,
    closeConnection,
    ConnectionPool,
    createRequest,
    Request,
    ConnectionError,
    RequestError,
    NVarChar
} from "../shared/databaseHandler";

import { BadRequestError } from "../shared/BadRequestError";

export async function httpGetProjects(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    
    let connection: ConnectionPool | null = null;

    try {

        // Retrieve the language preference from the Accept-Language header
        const language = request.headers.get('accept-language');
        if (!language)
            throw new BadRequestError("The 'Accept-Language' header is missing and is required for this request.");

        connection = await createConnection({
            user: process.env["MSSQL_API_READ_ONLY_USER"],
            password: process.env["MSSQL_API_READ_ONLY_USER_PASSWORD"]
        });

        const projectsRequest: Request = createRequest(connection);
        projectsRequest.input('language', NVarChar(8), language);
        const projectsResult = await projectsRequest.query(`
            SELECT
                [project].[projects].[id],
                [project].[projects].[name], 
                [project].[projects].[image], 
                [project].[projects].[repository],
                [project].[project_descriptions].[description],
                (
                    SELECT [project].[project_topics].[topic] 
                    FROM [project].[project_topics] 
                    WHERE [project].[project_topics].[project_id] = [portfolio].[project].[projects].[id]
                    FOR JSON PATH
                ) AS [topics]
            FROM [portfolio].[project].[projects]
            JOIN [project].[project_descriptions] 
            ON [project].[project_descriptions].[project_id] = [project].[projects].[id] AND [project].[project_descriptions].[language] = @language;`
        )

        const projects = [];

        for (const project of projectsResult.recordset) {
            const temp = {
                id: project.id,
                name: project.name,
                description: project.description,
                image: project.image,
                repository: project.repository,
                topics: JSON.parse(project.topics)
            }
            projects.push(temp);
        }

        return {
            status: 200,
            body: JSON.stringify(projects)
        }
    }
    catch (error) {

        if (error instanceof BadRequestError){
            context.log(`Something went wrong with the request:\n\t${error}`);
            return{
                status: 400,
                body: error.message
            }
        }
        if (error instanceof ConnectionError) {
            context.log(`Something went wrong with the database connection:\n\t${error}`);
            return{
                status: 500
            }
        }
        else if (error instanceof RequestError) {
            context.log(`Something went wrong with the database request:\n\t${error}`);
            return{
                status: 500
            }
        }

        context.log(`Something went wrong: ${error}`);
        return {
            status: 500
        };

    }
    finally {

        if (connection)
            closeConnection(connection);

    }
};

app.http('httpGetProjects', {
    methods: ['GET'],
    authLevel: 'function',
    handler: httpGetProjects,
    route: "projects"
});
