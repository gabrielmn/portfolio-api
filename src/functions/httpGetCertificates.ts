import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";
import {
    createConnection,
    closeConnection,
    ConnectionPool,
    createRequest,
    Request,
    ConnectionError,
    RequestError,
    NVarChar
} from "../shared/databaseHandler";

import { BadRequestError } from "../shared/BadRequestError";

export async function httpGetCertificates(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {

    let connection: ConnectionPool | null = null;

    try {

        // Retrieve the language preference from the Accept-Language header
        const language = request.headers.get('accept-language');
        if (!language)
            throw new BadRequestError("The 'Accept-Language' header is missing and is required for this request.");

        connection = await createConnection({
            user: process.env["MSSQL_API_READ_ONLY_USER"],
            password: process.env["MSSQL_API_READ_ONLY_USER_PASSWORD"]
        });

        const certificatesRequest: Request = createRequest(connection);
        certificatesRequest.input('language', NVarChar(8), language);
        const certificatesResult = await certificatesRequest.query(`
            SELECT 
            [certificate].[certificates].[id],
            [certificate].[certificates].[name],
            [certificate].[certificates].[conclusion_date],
            [certificate].[certificates].[link],
            (
                SELECT
                    [certificate].[certificate_topics].[id],
                    [certificate].[certificate_topics].[topic]
                FROM [certificate].[certificate_topics] 
                WHERE [certificate].[certificate_topics].[certificate_id] = [certificate].[certificates].[id] AND [certificate].[certificate_topics].[language] = @language
                FOR JSON PATH
            ) AS [topics]
            FROM [certificate].[certificates];
        `)

        const certificates = [];

        for (const certificate of certificatesResult.recordset) {
            const temp = {
                id: certificate.id,
                name: certificate.name,
                conclusionDate: certificate.conclusion_date,
                link: certificate.link,
                topics: JSON.parse(certificate.topics)
            };
            certificates.push(temp);
        }

        return {
            status: 200,
            body: JSON.stringify(certificates)
        }
    }
    catch (error) {

        if (error instanceof BadRequestError){
            context.log(`Something went wrong with the request:\n\t${error}`);
            return{
                status: 400,
                body: error.message
            }
        }
        if (error instanceof ConnectionError) {
            context.log(`Something went wrong with the database connection:\n\t${error}`);
            return{
                status: 500
            }
        }
        else if (error instanceof RequestError) {
            context.log(`Something went wrong with the database request:\n\t${error}`);
            return{
                status: 500
            }
        }

        context.log(`Something went wrong:\n\t${error}`);
        return {
            status: 500
        };

    }
    finally {

        if (connection)
            closeConnection(connection);

    }
};

app.http('httpGetCertificates', {
    methods: ['GET'],
    authLevel: 'function',
    handler: httpGetCertificates,
    route:'certificates'
});